import pandas as pd

# Load the CSV file into a DataFrame
csv_file_path = './test-dataset.csv'  # Replace with the path to your CSV file
df = pd.read_csv(csv_file_path)

# Combine "question" and "answer" columns into a new "text" column
df['text'] = df.apply(lambda row: f'<s>[INST] {row["input"]} [/INST] {row["output"]}</s>', axis=1)

# Drop the "question" and "answer" columns
df = df.drop(['input', 'output'], axis=1)

# Save the modified DataFrame to a new CSV file
output_csv_path = 'test-dataset.csv'  # Replace with the desired output file path
df.to_csv(output_csv_path, index=False)

print(f'The new CSV file "{output_csv_path}" has been created.')

