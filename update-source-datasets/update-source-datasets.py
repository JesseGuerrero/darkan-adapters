from scripts.utils import *

repoName = "DarkanRS/world-server"

extensions = {
    "Java": [".java"],
    "Kotlin": [".kt", ".kts"]
}

sourceDir = "./src"
sources = list_files(sourceDir)

trainDataset = []
splitIndex = int(len(sources)*0.8)
for source in sources:
    fileData = {}
    fileData["text"] = open(source[0], "r").read()
    # fileData["code"] = open(source[0], "r").read()
    # fileData["repo_name"] = repoName
    # fileData["path"] = source[0]
    # fileData["language"] = source[1]
    trainDataset.append(fileData)

testDataset = []
for source in sources[splitIndex:]:
    fileData = {}
    fileData["text"] = open(source[0], "r").read()
    # fileData["code"] = open(source[0], "r").read()
    # fileData["repo_name"] = repoName
    # fileData["path"] = source[0]
    # fileData["language"] = source[1]
    testDataset.append(fileData)


saveDataset("trainOLD.csv", trainDataset)
saveDataset("test.csv", testDataset)
