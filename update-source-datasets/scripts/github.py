import git
from pathlib import Path
import shutil

def gitCloneTemp(repository_url: str):
    tempSource = './excluded/source-temp'

    try:
        git.Repo.clone_from(repository_url, tempSource)
        print(f"Repository cloned successfully")
    except git.GitCommandError as e:
        print(f"Error cloning repository: {e}")

def _emptyTempFolder(tempSource: str):
    try:
        # Create a Path object for the folder
        folder = Path(tempSource)

        # Use shutil.rmtree to remove the entire folder and its contents
        shutil.rmtree(folder)

        # Recreate an empty folder
        folder.mkdir()
        print(f"Folder {tempSource} emptied successfully.")
    except Exception as e:
        print(f"Error emptying folder: {e}")