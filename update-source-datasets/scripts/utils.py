import os
import csv

def listFiles(directory) -> list[str]:
    global extensions
    allSources: list[str] = list()
    script_path = os.path.dirname(os.path.abspath(__file__))
    for root, dirs, files in os.walk(directory):
        for file in files:
            relative_path = os.path.relpath(os.path.join(root, file), script_path)
            # Check if the file has an allowed extension
            for lang, extension in extensions.items():
                if any(relative_path.endswith(ext) for ext in extension):
                    allSources.append((relative_path, lang))
    return allSources

def saveDataset(path: str, dataset: list) -> None:
    with open(path, mode='w', newline='') as file:
        # Define CSV headers based on dictionary keys
        fieldnames = dataset[0].keys()

        # Create a CSV writer object
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        # Write headers to the CSV file
        writer.writeheader()

        # Write data to the CSV file
        writer.writerows(dataset)